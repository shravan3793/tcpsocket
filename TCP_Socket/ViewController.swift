//
//  ViewController.swift
//  TCP_Socket
//
//  Created by Shravan Agrawal on 20/06/19.
//  Copyright © 2019 Shravan Agrawal. All rights reserved.
//

import UIKit
import SwiftSocket
class ViewController: UIViewController, UITableViewDataSource {

    
    //MARK:- outlets
    @IBOutlet weak var tblViw: UITableView!
    @IBOutlet weak var btnConnect: UIBarButtonItem!
    var client : TCPClient?
    let strAddress = "indition.cc"
    let port = 25
    var arrText : [(isServer:Bool,message:String)] = []
    
    //MARK: - view life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        btnConnect.title = ConnectionStatus.Connect.rawValue
        tblViw.tableFooterView = UIView()
        tblViw.estimatedRowHeight = 40
        tblViw.rowHeight = UITableView.automaticDimension
        // Do any additional setup after loading the view.
    }

    //MARK:- connect or disconnect the socket
    @IBAction func connectToServer(_ sender: UIBarButtonItem) {
        arrText = []
        if ConnectionStatus.Connect.rawValue == sender.title!
        {
            client = TCPClient(address: strAddress , port: Int32(port))
            switch client!.connect(timeout: -1) {
            case .success:
                 guard let data = client!.read(1024*10) else { return }
                 if let response = String(bytes: data, encoding: .utf8) {
                    arrText.append(((isServer: true, message: response)))
                     if !response.hasPrefix("220")
                     {
                        arrText.append((isServer: false, message: "Greeting Failed"))
                        closeConnection()
                     }else{
                        sender.title = ConnectionStatus.Disconnect.rawValue
                        arrText.append((isServer: false, message: "Connected to Indition.cc"))
                        arrText.append((isServer: false, message: "EHLO\r\n"))
                        tblViw.refreshData(arr: arrText)
                    
                        switch client!.send(string: "EHLO\r\n") {
                        case .success:
                            guard let data = client!.read(1024*10) else { return }
                            
                            if let response = String(bytes: data, encoding: .utf8) {
                                arrText.append((isServer: true, message: response))
                                tblViw.refreshData(arr: arrText)
                                
                                switch client!.send(string: "quit\r\n") {
                                case .success:
                                    guard let data = client!.read(1024*10) else { return }
                                    if let response = String(bytes: data, encoding: .utf8) {
                                        arrText.append((isServer: true, message: response))
                                        arrText.append((isServer: false, message: "disconnected from tagrem.cc"))
                                        tblViw.refreshData(arr: arrText)
                                    }
                                case .failure(let error):
                                    showAlert(strMessage: error.localizedDescription)
                                }
                                
                            }
                        case .failure(let error):
                            showAlert(strMessage: error.localizedDescription)
                        }
                    }
                 }
            case .failure(let error):
                showAlert(strMessage: error.localizedDescription)
            }
        }else{
            closeConnection()
        }
        
    }
    
    //close connection
    func closeConnection()
    {
        client?.close()
        btnConnect.title = ConnectionStatus.Connect.rawValue
        arrText = []
        tblViw.reloadData()
    }
    
    
    func showAlert(strMessage:String)
    {
        let alert = UIAlertController(title: "TCP Socket", message:strMessage , preferredStyle: .alert)
        let actOk = UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(actOk)
        self.present(alert, animated: true, completion: nil)
    }
    
    //MARK:- tableview datasource
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Communication Between Client-Server"
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrText.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if arrText[indexPath.row].isServer
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "serverCell")!
            let lbl = cell.viewWithTag(101) as! UILabel
            lbl.text = arrText[indexPath.row].message
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "clientCell")!
            let lbl = cell.viewWithTag(101) as! UILabel
            lbl.text = arrText[indexPath.row].message
            return cell
        }
    }
}
extension UITableView{
    
    func refreshData(arr:[Any])
    {
        self.reloadRows(at: [IndexPath(row: arr.count-1, section: 0)], with: .none)
    }
}
//enum for states of connection
enum ConnectionStatus : String{
    case Connect
    case Disconnect
}

